import { expect } from 'chai';
import { UsersController } from '../lib/controllers/users.controller';
import { checkStatusCode, checkResponseTime } from '../../helpers/functionsForChecking.helper';
import chai from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

const users = new UsersController();
const schemas = require('./data/schemas_testData.json');
const baseUrl = 'http://tasque.lol';

let userId: number;
let createdUser: any;
let userEmail: string;
let userPassword: string;
let authToken: string;
let refreshToken: string;
let updatedUserName: string;
let userInfoFromTokenBeforeUpdate: any;
let userInfoFromTokenAfterUpdate: any;

describe('User info', () => {
  before(async () => {
    // Create a new user
    const newUser: { avatar: string; email: string; userName: string; password: string } = {
      avatar: 'string',
      email: 'newuser09@gmail.com',
      userName: 'newuser09',
      password: 'password129',
    };

    const response = await chai.request(baseUrl).post('/api/Register').send(newUser);

    checkStatusCode(response, 201);

    createdUser = response.body.user;
    userId = createdUser.id;
    userEmail = newUser.email;
    userPassword = newUser.password;

    console.log('Created User ID:', userId);

    // Authenticate the user and save the authToken and refreshToken
    const authResponse = await chai
      .request(baseUrl)
      .post('/api/Auth/login')
      .send({
        email: userEmail,
        password: userPassword,
      });

    checkStatusCode(authResponse, 200);

    authToken = authResponse.body.token.accessToken.token;
    refreshToken = authResponse.body.token.refreshToken;

    // Get user info from token before updating the user
    const userInfoResponseBeforeUpdate = await chai
      .request(baseUrl)
      .get(`/api/Users/fromToken`)
      .set('Authorization', `Bearer ${authToken}`);

    checkStatusCode(userInfoResponseBeforeUpdate, 200);
    userInfoFromTokenBeforeUpdate = userInfoResponseBeforeUpdate.body;
  });

  it('User is created', async () => {
    expect(createdUser).to.exist;

    const response = await chai.request(baseUrl).get(`/api/Users/${userId}`);

    checkStatusCode(response, 200);

    expect(response.body.email).to.equal(createdUser.email);
    expect(response.body.userName).to.equal(createdUser.userName);
  });

  it(`Users list. Should return a status code 200`, async () => {
    const response = await chai.request(baseUrl).get('/api/Users');
    checkStatusCode(response, 200);
  });

  it(`Users list. Should return a non-empty response`, async () => {
    const response = await chai.request(baseUrl).get('/api/Users');
    expect(response).to.not.be.empty;
  });

  it('Get user info from token before updating user. Should return user info based on token', () => {
    expect(userInfoFromTokenBeforeUpdate).to.exist;
    expect(userInfoFromTokenBeforeUpdate.id).to.equal(userId);
    expect(userInfoFromTokenBeforeUpdate.email).to.equal(userEmail);
    expect(userInfoFromTokenBeforeUpdate.userName).to.equal(createdUser.userName);
  });

  it('Update User. Should update the user information', async () => {
    // Generate a random value for the userName using the custom function generateRandomString
    updatedUserName = generateRandomString(10);
  
    // Prepare the request body for updating the user
    const updatedUserData = {
      id: userId,
      avatar: 'updated_avatar',
      email: userEmail,
      userName: updatedUserName,
    };
  
    // Send a PUT request to update the user data
    const response = await chai
      .request(baseUrl)
      .put(`/api/Users`)
      .set('Authorization', `Bearer ${authToken}`)
      .send(updatedUserData);
  
    // Ensure that the update was successful with a status code of 204
    checkStatusCode(response, 204);
  
    console.log('Updated User Name:', updatedUserName); // Display the updated username in the console
  
    // Get user info by ID after updating the user
    const updatedUserInfoResponse = await chai.request(baseUrl).get(`/api/Users/${userId}`);
  
    checkStatusCode(updatedUserInfoResponse, 200);
  
    // Verify that the user info has been updated with the new username
    expect(updatedUserInfoResponse.body.id).to.equal(userId);
    expect(updatedUserInfoResponse.body.email).to.equal(userEmail);
    expect(updatedUserInfoResponse.body.userName).to.equal(updatedUserName);
  
    // Get user info from token after updating the user
    const userInfoResponseAfterUpdate = await chai
      .request(baseUrl)
      .get(`/api/Users/fromToken`)
      .set('Authorization', `Bearer ${authToken}`);
  
    checkStatusCode(userInfoResponseAfterUpdate, 200);
    userInfoFromTokenAfterUpdate = userInfoResponseAfterUpdate.body;
  });

  it('Get user info from token after updating user. Should return user info based on updated token', () => {
    expect(userInfoFromTokenAfterUpdate).to.exist;
    expect(userInfoFromTokenAfterUpdate.id).to.equal(userId);
    expect(userInfoFromTokenAfterUpdate.email).to.equal(userEmail);
    expect(userInfoFromTokenAfterUpdate.userName).to.equal(updatedUserName);
  });

  it('Get User by ID. Should return user info by ID and match with saved info', async () => {
    const response = await chai.request(baseUrl).get(`/api/Users/${userId}`);

    checkStatusCode(response, 200);

    expect(response.body.id).to.equal(userId);
    expect(response.body.email).to.equal(userEmail);
    expect(response.body.userName).to.equal(updatedUserName);
    
  });

  it('Delete User by ID. Should delete the user and return a status code of 200', async () => {
    const response = await chai
      .request(baseUrl)
      .delete(`/api/Users/${userId}`)
      .set('Authorization', `Bearer ${authToken}`);

    checkStatusCode(response, 204);
  });
});

function generateRandomString(length: number): string {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    result += characters[randomIndex];
  }
  return result;
}
